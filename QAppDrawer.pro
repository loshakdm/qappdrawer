#-------------------------------------------------
#
# Project created by QtCreator 2016-10-23T15:10:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QAppDrawer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    appbutton.cpp

HEADERS  += mainwindow.h \
    appbutton.h

FORMS    += mainwindow.ui

# path on device
target.path = /tmp/pitest
INSTALLS += target

appCatalog.path = /tmp/pitest
appCatalog.files = apps.ini
INSTALLS += appCatalog
