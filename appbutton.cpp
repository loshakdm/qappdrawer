#include "appbutton.h"

#include <QPainter>
#include <QPaintEvent>
#include <QDebug>
#include <QStaticText>

AppButton::AppButton(QWidget *parent) : QAbstractButton(parent),
    m_buttonText(""),
    m_buttonIcon(QIcon())
{
}

void AppButton::setText(const QString &text)
{
    m_buttonText = text;
}

void AppButton::setIcon(const QIcon &icon)
{
    m_buttonIcon = icon;
}

void AppButton::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);
    p.setPen(Qt::white); /// TODO: Let the user set this
    p.drawText(0, 0, 128, 128, Qt::AlignBottom | Qt::AlignHCenter, m_buttonText);

    int buttonX = (this->width() / 2) - (this->width() * 0.75) / 2; // totalWidth / 2 - buttonWidth / 2
    m_buttonIcon.paint(&p, buttonX, 0, this->width() * 0.75, this->height() * 0.75);
}
