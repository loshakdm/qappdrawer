#ifndef APPBUTTON_H
#define APPBUTTON_H

#include <QAbstractButton>

class AppButton : public QAbstractButton
{
public:
    explicit AppButton(QWidget *parent = Q_NULLPTR);

    void setText(const QString &text);
    void setIcon(const QIcon &icon);

protected:
    void paintEvent(QPaintEvent *);

    QString m_buttonText;
    QIcon m_buttonIcon;
};

#endif // APPBUTTON_H
