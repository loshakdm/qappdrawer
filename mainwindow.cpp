#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QDebug>
#include <QProcess>
#include <QPushButton>

#include "appbutton.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    currentPage = ui->buttonContainer0;

    if (readCatalog()) {
        int rows, columns;
        nextButtonX = nextButtonY = 0;
        buttonHeight = 128;
        buttonWidth = 128;

        rows = currentPage->height() / buttonHeight;
        columns = currentPage->width() / buttonWidth;

        drawerDimensions = qMakePair(rows, columns);

        drawButtons();

        QString bgFilePath = qApp->applicationDirPath();
        bgFilePath.append("/");
        bgFilePath.append("bg.jpg");

        if (QFile(bgFilePath).exists()) { // Maybe don't bother to check for it's existence?
                QPixmap bkgnd(bgFilePath);
                QPalette palette;
                palette.setBrush(QPalette::Background, bkgnd);
                this->setPalette(palette);
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::readCatalog()
{
    if (catalogFilePath.isEmpty()) { /// TODO: Read the first cmd line arg to QAppDrawer for catalog file path
        catalogFilePath = qApp->applicationDirPath();
        catalogFilePath.append("/");
        catalogFilePath.append("apps.ini");
    }

    QFile catalogFile(catalogFilePath);

    if (catalogFile.open(QIODevice::ReadOnly)) {
        QString catalogEntry;
        QStringList appDetails;
        AppButton *appButton;

        while (!catalogFile.atEnd()) {
            catalogEntry = catalogFile.readLine();

            if (catalogEntry.startsWith('#') || isEmpty(catalogEntry)) {
                continue;
            }

            if (catalogEntry.endsWith("\n")) // Strip off the \n for proper parsing
                catalogEntry = catalogEntry.left(catalogEntry.length() - 1);

            appDetails = catalogEntry.split(';');

            if (appDetails.length() != 3) { // Doesn't break the whole read process over one error
                qDebug() << "Error in catalog file at this line: " << catalogEntry;

                continue;
            }

            // If we get this far, go ahead and create an appsMap entry + QPushButton
            appButton = new AppButton(currentPage);

            appsMap.insert(appButton, appDetails);

            connect(appButton, SIGNAL(clicked()), this, SLOT(openApp()));
        }

        if (appsMap.isEmpty()) {
            qDebug() << "Error: Empty catalog file.";

            return false;
        }

    } else {
        qDebug() << "Error: Issue opening app catalog.";

        return false;
    }

    return true;
}

/// TODO: Implement multiple page functionality
void MainWindow::drawButtons()
{
    QHashIterator<AppButton*, QStringList> appsIt(appsMap);
    AppButton *currentButton;

    while (appsIt.hasNext()) {
        appsIt.next();

        currentButton = appsIt.key();
        currentButton->setGeometry(nextButtonX * buttonWidth, nextButtonY * buttonHeight, buttonWidth, buttonHeight);
        currentButton->setText(appsIt.value().first());
        currentButton->setIcon(QIcon(appsIt.value().at(1)));

        nextButtonX++;
    }
}

void MainWindow::Exit() /// TODO: Add clean-up here
{
    exit(0);
}

void MainWindow::openApp()
{
    AppButton *selectedApp = dynamic_cast<AppButton*>(sender());

    QString mapEntry = appsMap[selectedApp].last();

    // Handle special operations
    if (mapEntry.startsWith('$')) {
        if (mapEntry.endsWith("exit")) {
            Exit();
        }
    }

    runningApp = new QProcess(this);

    runningApp->start(mapEntry);

    connect(runningApp, SIGNAL(finished(int)), this, SLOT(closedApp(int)));

    this->hide();
}

void MainWindow::closedApp(int exitCode)
{
    Q_UNUSED(exitCode); /// TODO: Use this for debug

//    delete runningApp;

    this->show();
}

bool MainWindow::isEmpty(QString s)
{
    if (s.isEmpty())
        return true;

    if (s.count(QChar(32)) == s.length())
        return true;

    if (s.startsWith('\n'))
        return true;

    return false;
}
