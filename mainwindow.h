#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class AppButton;
class QGridLayout;
class QProcess;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void openApp();
    void closedApp(int exitCode);
    void Exit();

    /// TODO: Add shutdown functionality

private:
    Ui::MainWindow *ui;
    QHash<AppButton*, QStringList> appsMap;
    QMenu *fileMenu;
    QProcess *runningApp;

    QWidget *currentPage;
    int buttonWidth;
    int buttonHeight;
    QPair<int, int> drawerDimensions;

    int nextButtonX;
    int nextButtonY;

    QString catalogFilePath;

    bool readCatalog();
    void drawButtons();
    bool isEmpty(QString s);
};

#endif // MAINWINDOW_H
